import React, { useState, useEffect } from "react";
import styled from "@emotion/styled";
import { css } from "@emotion/core";
import Router from "next/router";

const InputText = styled.input`
	border: 1px solid var(--gris3);
	padding: 1rem;
	min-width: 300px;
`;
const BtnSubmit = styled.button`
	background-color: Transparent;
	background-repeat: no-repeat;
	height: 3rem;
	width: 3rem;
	display: block;
	background-size: 4rem;
	background-image: url("/static/img/buscar.png");
	background-position: center;
	position: absolute;
	right: 1rem;
	top: 0.5rem;
	background-color: none;
	border: none;
	text-indent: -9999px;

	&:hover {
		cursor: pointer;
	}
`;

const Search = () => {
	const [value, setValue] = useState("");

	const handleSubmit = (e) => {
		e.preventDefault();
		if (value.trim() === "") return;
		//redirect user to /buscar
		Router.push({
			pathname: "/search",
			query: { q: value },
		});
	};

	return (
		<form
			css={css`
				position: relative;
			`}
			onSubmit={handleSubmit}
		>
			<InputText type="text" name="find" placeholder="Buscar productos..." onChange={(e) => setValue(e.target.value)} />
			<BtnSubmit>Search</BtnSubmit>
		</form>
	);
};

export default Search;
