import React, { useContext } from "react";
import Link from "next/link";
import styled from "@emotion/styled";
import { css } from "@emotion/core";
import Search from "../ui/Search";
import NavBar from "./NavBar";
import CustomButton from "../ui/CustomButton";
import { FirebaseContext } from "../../firebase";
import { useRouter } from "next/router";

const ContenedorHeader = styled.div`
	max-width: 1200px;
	width: 95%;
	margin: 0 auto;
	@media (min-width: 768px) {
		display: flex;
		justify-content: space-between;
	}
`;

const PLogo = styled.a`
	color: var(--naranja);
	font-size: 4rem;
	line-height: 0;
	font-weight: 700;
	font-family: "Roboto Slab", serif;
	margin-right: 2rem;

	&:hover {
		cursor: pointer;
	}
`;

const Header = () => {
	const { authUser, firebase } = useContext(FirebaseContext);
	const router = useRouter();

	const handleLogout = () => {
		firebase.logout();
		router.push("/");
	};
	return (
		<header
			css={css`
				border-bottom: 2px solid var(--gris3);
				padding: 1rem 0;
			`}
		>
			<ContenedorHeader>
				<div
					css={css`
						display: flex;
						align-items: center;
					`}
				>
					<Link href="/">
						<PLogo>P</PLogo>
					</Link>
					<Search />

					<NavBar />
				</div>

				<div
					css={css`
						display: flex;
						align-items: center;
					`}
				>
					{authUser ? (
						<>
							<p
								css={css`
									margin-right: 2rem;
								`}
							>
								Usuario: {authUser.displayName}
							</p>
							<CustomButton bgColor="true" onClick={handleLogout}>
								Cerrar Sesión
							</CustomButton>
						</>
					) : (
						<>
							<Link href="/login">
								<CustomButton bgColor="true">Login</CustomButton>
							</Link>
							<Link href="/create-account">
								<CustomButton>Crear cuenta</CustomButton>
							</Link>
						</>
					)}
				</div>
			</ContenedorHeader>
		</header>
	);
};

export default Header;
