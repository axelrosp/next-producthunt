import React from "react";
import styled from "@emotion/styled";
import formatDistanceToNow from "date-fns/formatDistanceToNow";
import { es } from "date-fns/locale";
import Link from "next/link";

const LiProduct = styled.li`
	padding: 4rem;
	display: flex;
	justify-content: space-between;
	align-items: center;
	border-bottom: 1px solid #e1e1e1;
`;

const DivDescription = styled.div`
	flex: 0 1 600px;
	display: grid;
	grid-template-columns: 1fr 3fr;
	column-gap: 2rem;
`;

const Title = styled.a`
	font-size: 2rem;
	font-weight: bold;
	margin: 0;
	:hover {
		cursor: pointer;
	}
`;

const Description = styled.p`
	font-size: 1.6rem;
	margin: 0;
	color: #888;
`;

const DivComents = styled.div`
	margin-top: 2rem;
	display: flex;
	align-items: center;
	div {
		display: flex;
		align-items: center;
		border: 1px solid #e1e1e1;
		padding: 0.3rem 1rem;
		margin-right: 2rem;
	}
	img {
		width: 2rem;
		margin-right: 1rem;
	}
	p {
		font-size: 1.6rem;
		margin-right: 1rem;
		font-weight: 700;
		&:last-of-type {
			margin: 0;
		}
	}
`;

const Img = styled.img`
	width: 200px;
`;

const DivVotos = styled.div`
	flex: 0 0 auto;
	text-align: center;
	border: 1px solid #e1e1e1;
	padding: 1rem 3rem;
	div {
		font-size: 2rem;
	}
	p {
		margin: 0;
		font-size: 2rem;
		font-weight: 700;
	}
`;

const Product = ({ product }) => {
	const { id, comentarios, creado, descripcion, empresa, nombre, url, imageUrl, votos } = product;
	return (
		<LiProduct>
			<DivDescription>
				<div>
					<Img src={imageUrl} />
				</div>
				<div>
					<Link href="/products/[id]" as={`/products/${id}`}>
						<Title>{nombre}</Title>
					</Link>
					<Description>{descripcion}</Description>
					<DivComents>
						<div>
							<img src="/static/img/comentario.png" />
							<p>{comentarios.length} Comentarios</p>
						</div>
					</DivComents>

					<p>Publicado hace: {formatDistanceToNow(new Date(creado), { locale: es })}</p>
				</div>
			</DivDescription>

			<DivVotos>
				<div>&#9650;</div>
				<p>{votos}</p>
			</DivVotos>
		</LiProduct>
	);
};

export default Product;
