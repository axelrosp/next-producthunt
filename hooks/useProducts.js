import React, { useState, useEffect, useContext } from "react";
import { FirebaseContext } from "../firebase";

const useProducts = (order) => {
	const [products, setProducts] = useState([]);
	const { firebase } = useContext(FirebaseContext);

	useEffect(() => {
		const getProducts = () => {
			return firebase.db.collection("products").orderBy(order, "desc").onSnapshot(snapshotManager);
		};
		getProducts();
	}, []);

	function snapshotManager(snapshot) {
		const products = snapshot.docs.map((doc) => {
			return {
				id: doc.id,
				...doc.data(),
			};
		});
		setProducts(products);
	}

	return { products };
};

export default useProducts;
