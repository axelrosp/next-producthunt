export default function validateCreateProduct(values) {
	let errors = {};

	if (!values.nombre) {
		errors.name = "El nombre es obligatorio";
	}

	if (!values.empresa) {
		errors.empresa = "La empresa es obligatoria";
	}

	if (!values.url) {
		errors.url = "La url es obligatoria";
	} else if (!/^(ftp|http|https):\/\/[^ "]+$/.test(values.url)) {
		errors.url = "Url mal formateada o no valida";
	}

	if (!values.descripcion) {
		errors.descripcion = "La descripcion es obligatoria";
	}

	return errors;
}
