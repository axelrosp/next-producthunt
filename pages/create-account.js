import React, { useState } from "react";
import { css } from "@emotion/core";
import Router from "next/router";
import Layout from "../components/layout/layout";
import { Form, Field, InputSubmit, Error } from "../components/ui/Form";

import firebase from "../firebase";

import useValidation from "../hooks/useValidation";
import validateCreateAccount from "../validation/validateCreateAccount";

const INITIAL_STATE = {
	nombre: "",
	email: "",
	password: "",
};

const CreateAccount = () => {
	const [error, setError] = useState(false);

	const { values, errors, handleSubmit, handleChange, handleBlur } = useValidation(INITIAL_STATE, validateCreateAccount, createAccount);

	const { nombre, email, password } = values;

	async function createAccount() {
		try {
			await firebase.register(nombre, email, password);
			Router.push("/");
		} catch (error) {
			console.error("Error al crear user", error.message);
			setError(error.message);
		}
	}

	return (
		<div>
			<Layout>
				<>
					<h1
						css={css`
							text-align: center;
							margin-top: 5rem;
						`}
					>
						Crear cuenta
					</h1>
					<Form onSubmit={handleSubmit} noValidate>
						<Field>
							<label htmlFor="nombre">Nombre</label>
							<input type="text" id="nombre" placeholder="nombre" name="nombre" value={nombre} onChange={handleChange} onBlur={handleBlur} />
						</Field>
						{errors.name && <Error>{errors.name}</Error>}
						<Field>
							<label htmlFor="email">Email</label>
							<input type="email" id="email" placeholder="Email" name="email" value={email} onChange={handleChange} onBlur={handleBlur} />
						</Field>
						{errors.email && <Error>{errors.email}</Error>}
						<Field>
							<label htmlFor="password">Password</label>
							<input
								type="password"
								id="password"
								placeholder="Password"
								name="password"
								value={password}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
						</Field>
						{errors.password && <Error>{errors.password}</Error>}
						{error && <Error>{error}</Error>}
						<InputSubmit type="submit" value="Crear cuenta" />
					</Form>
				</>
			</Layout>
		</div>
	);
};
export default CreateAccount;
