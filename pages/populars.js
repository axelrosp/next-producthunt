import React from "react";
import Layout from "../components/layout/layout";
import Product from "../components/layout/Product";
import useProducts from "../hooks/useProducts";

const Populars = () => {
	const { products } = useProducts("votos");

	return (
		<div>
			<Layout>
				<div className="listado-productos">
					<div className="contenedor">
						<ul className="bg-white">
							{products.map((p) => (
								<Product key={p.id} product={p} />
							))}
						</ul>
					</div>
				</div>
			</Layout>
		</div>
	);
};

export default Populars;
