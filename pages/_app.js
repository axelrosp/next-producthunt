import App from "next/app";
import firebase, { FirebaseContext } from "../firebase";
import useAuth from "../hooks/useAuth";

const MyApp = (props) => {
	const authUser = useAuth();
	console.log(authUser);
	const { Component, pageProps } = props;

	return (
		<FirebaseContext.Provider value={{ firebase, authUser }}>
			<Component {...pageProps} />
		</FirebaseContext.Provider>
	);
};

export default MyApp;
