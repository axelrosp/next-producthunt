import React, { useState, useContext } from "react";
import { css } from "@emotion/core";
import Router, { useRouter } from "next/router";
import FileUploader from "react-firebase-file-uploader";
import Layout from "../components/layout/layout";
import { Form, Field, InputSubmit, Error } from "../components/ui/Form";

import { FirebaseContext } from "../firebase";

import useValidation from "../hooks/useValidation";
import validateCreateProduct from "../validation/validateCreateProduct";
import Error404 from "../components/layout/404";

const INITIAL_STATE = {
	nombre: "",
	empresa: "",
	url: "",
	descripcion: "",
};

const NewProduct = () => {
	//state de las imagenes
	const [imageName, setImageName] = useState("");
	const [uploading, setUploading] = useState(false);
	const [progress, setProgress] = useState(0);
	const [imageUrl, setImageUrl] = useState("");

	const [error, setError] = useState(false);

	const { values, errors, handleSubmit, handleChange, handleBlur } = useValidation(INITIAL_STATE, validateCreateProduct, createProduct);

	const { nombre, empresa, imagen, url, descripcion } = values;

	//hook routing para redireccionar
	const router = useRouter();

	//Context con CRUD de firebase
	const { authUser, firebase } = useContext(FirebaseContext);

	async function createProduct() {
		if (!authUser) {
			return router.push("/login");
		}

		//Crear el objeto de neuvo producto
		const product = {
			nombre,
			empresa,
			url,
			imageUrl,
			descripcion,
			votos: 0,
			comentarios: [],
			creado: Date.now(),
			creador: {
				id: authUser.uid,
				nombre: authUser.displayName,
			},
			hanVotado: [],
		};

		//Insertar en la base de datos
		await firebase.db.collection("products").add(product);
		return router.push("/");
	}

	//Uploag image methods
	const handleUploadStart = () => {
		setProgress(0);
		setUploading(true);
	};

	const handleProgress = (progress) => {
		setProgress({ progress });
	};

	const handleUploadError = (error) => {
		setUploading(error);
		console.error(error);
	};

	const handleUploadSuccess = (name) => {
		setProgress(100);
		setUploading(false);
		setImageName(name);
		firebase.storage
			.ref("products")
			.child(name)
			.getDownloadURL()
			.then((url) => {
				setImageUrl(url);
				console.log(url);
			});
	};

	return (
		<div>
			<Layout>
				{!authUser ? (
					<Error404 />
				) : (
					<>
						<h1
							css={css`
								text-align: center;
								margin-top: 5rem;
							`}
						>
							Nuevo Producto
						</h1>
						<Form onSubmit={handleSubmit} noValidate>
							<fieldset>
								<legend>Información general</legend>
								<Field>
									<label htmlFor="nombre">Nombre</label>
									<input
										type="text"
										id="nombre"
										placeholder="Nombre del producto"
										name="nombre"
										value={nombre}
										onChange={handleChange}
										onBlur={handleBlur}
									/>
								</Field>
								{errors.name && <Error>{errors.name}</Error>}

								<Field>
									<label htmlFor="empresa">Empresa</label>
									<input
										type="text"
										id="empresa"
										placeholder="Empresa proveedora"
										name="empresa"
										value={empresa}
										onChange={handleChange}
										onBlur={handleBlur}
									/>
								</Field>
								{errors.empresa && <Error>{errors.empresa}</Error>}

								<Field>
									<label htmlFor="imagen">Imagen</label>
									<FileUploader
										acept="img/*"
										id="imagen"
										name="imagen"
										randomizeFilename
										storageRef={firebase.storage.ref("products")}
										onUploadStart={handleUploadStart}
										onUploadError={handleUploadError}
										onUploadSuccess={handleUploadSuccess}
										onProgress={handleProgress}
									/>
								</Field>

								<Field>
									<label htmlFor="url">URL</label>
									<input type="url" id="url" name="url" placeholder="URL producto" value={url} onChange={handleChange} onBlur={handleBlur} />
								</Field>
								{errors.url && <Error>{errors.url}</Error>}
							</fieldset>
							<fieldset>
								<legend>Sobre el producto</legend>
								<Field>
									<label htmlFor="descripcion">Descripción</label>
									<textarea id="descripcion" name="descripcion" value={descripcion} onChange={handleChange} onBlur={handleBlur} />
								</Field>
								{errors.descripcion && <Error>{errors.descripcion}</Error>}
							</fieldset>

							{error && <Error>{error}</Error>}
							<InputSubmit type="submit" value="Crear producto" />
						</Form>
					</>
				)}
			</Layout>
		</div>
	);
};
export default NewProduct;
