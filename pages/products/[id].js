import React, { useEffect, useContext, useState } from "react";
import { useRouter } from "next/router";
import formatDistanceToNow from "date-fns/formatDistanceToNow";
import { es } from "date-fns/locale";

import { FirebaseContext } from "../../firebase";
import Layout from "../../components/layout/Layout";
import Error404 from "../../components/layout/404";
import { css } from "@emotion/core";
import styled from "@emotion/styled";
import { Form, Field, InputSubmit } from "../../components/ui/Form";
import CustomButton from "../../components/ui/CustomButton";

const ProductContainer = styled.div`
	@media (min-width: 768px) {
		display: grid;
		grid-template-columns: 2fr 1fr;
		column-gap: 2rem;
	}
`;

const CreadorProducto = styled.p`
	padding: 0.5rem 2rem;
	background-color: #da552f;
	color: #fff;
	text-transform: uppercase;
	font-weight: bold;
	display: inline-block;
	text-align: center;
`;

const Product = () => {
	const [product, setProduct] = useState({});
	const { comentarios, creado, descripcion, empresa, nombre, url, imageUrl, votos, hanVotado, creador } = product;

	const [error, setError] = useState(false);
	const [comment, setComment] = useState("");
	const [queryDB, setQueryDB] = useState(true);

	const router = useRouter();
	const { firebase, authUser } = useContext(FirebaseContext);

	const {
		query: { id },
	} = router;

	useEffect(() => {
		if (id && queryDB) {
			const getProduct = async () => {
				const productQuery = await firebase.db.collection("products").doc(id);
				const product = await productQuery.get();
				if (product.exists) {
					setError(false);
					setProduct(product.data());
				} else {
					setError(true);
				}
				setQueryDB(false);
			};
			getProduct();
		}
	}, [id]);

	const handleVoteProduct = () => {
		if (!authUser) return router.push("/login");
		if (hanVotado.includes(authUser.uid)) return;

		const newVotos = votos + 1;
		const newHanVotado = [...hanVotado, authUser.uid];

		firebase.db.collection("products").doc(id).update({ votos: newVotos, hanVotado: newHanVotado });

		setProduct({ ...product, votos: newVotos });
		setQueryDB(true);
	};

	const handleCommentChange = (e) => {
		setComment(e.target.value);
	};

	const handleCommentSubmit = (e) => {
		e.preventDefault();
		if (!authUser) return router.push("/login");
		if (!comment) return;
		const completedComment = { mensaje: comment, user: { uid: authUser.uid, displayName: authUser.displayName } };
		const newComments = [...comentarios, completedComment];

		console.log("COMENTARIO COMPLETO", newComments);

		firebase.db.collection("products").doc(id).update({ comentarios: newComments });
		setProduct({ ...product, comentarios: newComments });
		setQueryDB(true);
	};

	const handleRemoveProduct = async () => {
		if (!authUser) return router.push("/login");
		if (creador.id !== authUser.uid) return router.push("/");
		try {
			await firebase.db.collection("products").doc(id).delete();
			router.push("/");
		} catch (error) {
			console.error(error);
		}
	};

	const isOwner = (id) => {
		return creador.id === id;
	};

	const canRemove = () => {
		return authUser && isOwner(authUser.uid);
	};

	if (Object.keys(product).length === 0 && !error) return "Cargando ...";

	return (
		<Layout>
			<>
				{error ? (
					<Error404 />
				) : (
					<div className="contenedor">
						<h1
							css={css`
								text-align: center;
							`}
						>
							{nombre}
						</h1>

						<ProductContainer>
							<div>
								<p>Publicado hace: {formatDistanceToNow(new Date(creado), { locale: es })}</p>
								<p>
									Por {creador.nombre} de {empresa}
								</p>
								<img src={imageUrl} />
								<p>{descripcion}</p>
								{authUser && (
									<>
										<h2>Agrega tu comentario</h2>
										<form onSubmit={handleCommentSubmit}>
											<Field>
												<input type="text" name="mensaje" onChange={handleCommentChange} />
											</Field>
											<InputSubmit type="submit" value="Agregar comentario" />
										</form>
									</>
								)}
								<h2
									css={css`
										margin: 2rem 0;
									`}
								>
									Comentarios
								</h2>
								{comentarios.length === 0 ? (
									"Aún no hay comentarios"
								) : (
									<ul>
										{comentarios.map((c, i) => (
											<li
												key={`${c.user.uid}-${i}`}
												css={css`
													border: 1px solid #e1e1e1;
													padding: 2rem;
												`}
											>
												<p>{c.mensaje}</p>
												<p>
													Escrito por:
													<span
														css={css`
															font-weight: bold;
														`}
													>
														{" "}
														{c.user.displayName}
													</span>
												</p>
												{isOwner(c.user.uid) && <CreadorProducto>Owner</CreadorProducto>}
											</li>
										))}
									</ul>
								)}
							</div>

							<aside>
								<CustomButton target="_blank" bgColor="true" href={url}>
									Visitar URL
								</CustomButton>

								<div
									css={css`
										margin-top: 5rem;
									`}
								>
									<p
										css={css`
											text-align: center;
										`}
									>
										{votos} Votos
									</p>
									{authUser && <CustomButton onClick={handleVoteProduct}> Votar </CustomButton>}
								</div>
							</aside>
						</ProductContainer>
						{canRemove() && <CustomButton onClick={handleRemoveProduct}>Eliminar producto</CustomButton>}
					</div>
				)}
			</>
		</Layout>
	);
};

export default Product;
