import React, { useState, useEffect, useContext } from "react";
import styled from "@emotion/styled";
import Layout from "../components/layout/layout";
import { FirebaseContext } from "../firebase";
import Product from "../components/layout/Product";
import useProducts from "../hooks/useProducts";

const Home = () => {
	const { products } = useProducts("creado");
	return (
		<div>
			<Layout>
				<div className="listado-productos">
					<div className="contenedor">
						<ul className="bg-white">
							{products.map((p) => (
								<Product key={p.id} product={p} />
							))}
						</ul>
					</div>
				</div>
			</Layout>
		</div>
	);
};

export default Home;
