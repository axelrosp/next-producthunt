import React, { useEffect, useState } from "react";
import Layout from "../components/layout/layout";
import { useRouter } from "next/router";
import useProducts from "../hooks/useProducts";
import Product from "../components/layout/Product";

const Search = () => {
	const [result, setResult] = useState([]);
	const router = useRouter();
	const {
		query: { q },
	} = router;

	const { products } = useProducts("creado");

	useEffect(() => {
		const search = q.toLowerCase();
		const filter = products.filter((p) => p.nombre.toLowerCase().includes(search) || p.descripcion.toLowerCase().includes(search));
		setResult(filter);
	}, [q, products]);

	return (
		<div>
			<Layout>
				<div className="listado-productos">
					<div className="contenedor">
						<ul className="bg-white">
							{result.map((p) => (
								<Product key={p.id} product={p} />
							))}
						</ul>
					</div>
				</div>
			</Layout>
		</div>
	);
};

export default Search;
