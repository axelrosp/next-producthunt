import React, { useState } from "react";
import { css } from "@emotion/core";
import Layout from "../components/layout/layout";
import { Form, Field, InputSubmit, Error } from "../components/ui/Form";

import firebase from "../firebase";

import useValidation from "../hooks/useValidation";
import validateLogin from "../validation/validateLogin";
import { useRouter } from "next/router";

const INITIAL_STATE = {
	email: "",
	password: "",
};

const Login = () => {
	const [error, setError] = useState(false);

	const router = useRouter();

	const { values, errors, handleSubmit, handleChange, handleBlur } = useValidation(INITIAL_STATE, validateLogin, login);

	const { email, password } = values;

	async function login() {
		try {
			await firebase.login(email, password);
			router.push("/");
		} catch (error) {
			console.error("Error al crear user", error.message);
			setError(error.message);
		}
	}

	return (
		<div>
			<Layout>
				<>
					<h1
						css={css`
							text-align: center;
							margin-top: 5rem;
						`}
					>
						Iniciar Sesión
					</h1>
					<Form onSubmit={handleSubmit} noValidate>
						<Field>
							<label htmlFor="email">Email</label>
							<input type="email" id="email" placeholder="Email" name="email" value={email} onChange={handleChange} onBlur={handleBlur} />
						</Field>
						{errors.email && <Error>{errors.email}</Error>}
						<Field>
							<label htmlFor="password">Password</label>
							<input
								type="password"
								id="password"
								placeholder="Password"
								name="password"
								value={password}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
						</Field>
						{errors.password && <Error>{errors.password}</Error>}
						{error && <Error>{error}</Error>}
						<InputSubmit type="submit" value="Login" />
					</Form>
				</>
			</Layout>
		</div>
	);
};

export default Login;
